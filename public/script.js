import React from 'react';
import ReactDOM from 'react-dom';
import './styles.css';

class App extends React.Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.state = {
			author: 'autor',
			body: 'Wciśnij GENERUJ by wyświetlić cytat'
		};
	}

	render() {

		return (
			<div id="field">
				<div id="quoteGroup">
					<h3 id="quote">{this.state.body}</h3>
					<h5 id="author">~ {this.state.author}</h5>
				</div>
				<form id="form" onSubmit={this.handleSubmit}>
					<select name="cat" ref="cat">
						<option value="motivation" ref="cat">
							Motivation
						</option>
					</select>
					<input type="submit" value="GENERUJ" />
				</form>
			</div>
		);
	}
	handleSubmit(e) {
		e.preventDefault();
		var cat = this.refs.cat.value;

		fetch(`/api/quotes/?cat=${cat}`)
			.then(results => {
				return results.json();
			})
			.then(data => {
				let random = Math.floor(Math.random() * data.length-1) + 1;
				this.setState({					
					author: data[random].author,
					body: data[random].body
				});
			});
	}
}

ReactDOM.render(<App />, document.getElementById('root'));
