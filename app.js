const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const keys = require('./config/key');
const app = express();

mongoose.connect(keys.mongoURI, {useMongoClient: true});
mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname,'public')));

//ROUTES
app.get('/', (req, res) => {
	res.render('index');
});
app.use('/api', require('./routes/api'));

//Errors handling
app.use((err, req, res, next) => {
	console.error(err.message);
	res.status(422).send({error: err.message});
});

app.listen(process.env.PORT || 5000, () => {
	console.info('Server is running');
});

