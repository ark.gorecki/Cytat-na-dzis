const express = require('express');
const router = express.Router();
const Quote = require('../models/quotes');

router.get('/quotes', (req, res, next) => {
	Quote.find({'category':req.query.cat})
		.then( quotes => {
			res.send(quotes);
		});
});

router.post('/quotes', (req, res, next) => {
	let quote = new Quote(req.body);
	quote
		.save()
		.then(quote => {
			res.send(quote);
		})
		.catch(next);
});

router.put('/quotes/:id', (req, res, next) => {
	const id = req.params.id;
	Quote.findByIdAndUpdate({ _id: id }, req.body)
		.then(() => {
			Quote.findOne({ _id: id }).then(quote => {
				res.send(quote);
			});
		})
		.catch(next);
});

router.delete('/quotes/:id', (req, res, next) => {
	const id = req.params.id;
	Quote.findByIdAndRemove({ _id: id })
		.then(quote => {
			res.send(quote);
		})
		.catch(next);
});

module.exports = router;
