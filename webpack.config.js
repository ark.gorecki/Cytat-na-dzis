const path = require('path');
module.exports = {
	//entry point
	entry: path.resolve(__dirname, 'public') + '\\script.js',

	//output point
	output: {
		path: path.resolve(__dirname, 'public\\dist'),
		filename: 'bundle.js'
	},

	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'es2015']
				}
			},
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader',
			}
		]
	}
};
